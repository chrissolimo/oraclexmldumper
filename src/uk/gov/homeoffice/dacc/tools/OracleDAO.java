package uk.gov.homeoffice.dacc.tools;

import java.io.*;
import java.sql.*;

public class OracleDAO {

    private Connection con = null;
    private ResultSet rs = null;
    private PreparedStatement stmt = null;
    private String host = "";
    private String username = "";
    private String password = "";

    public OracleDAO(String h, String u, String p) {
        this.host = h;
        this.username = u;
        this.password = p;
    }

    public void connectDB() {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            String message = "Where is your Oracle JDBC Driver?: ";
            System.out.println(message + e.getMessage());
            return;
        }
        System.out.println("Oracle JDBC Driver Registered!");

        try {
            con = DriverManager.getConnection(this.host, this.username, this.password);
        } catch (SQLException e) {
            String message = "Error connecting to the database: ";
            System.out.println(message + e.getMessage());
        }
    }

    public void prepareStatement(String q) {
        try {
            stmt = con.prepareStatement(q);

            rs = stmt.executeQuery();

        } catch (SQLException e) {
            String message = "Error executing or preparing SQL statement: ";
            System.out.println(message + e.getMessage());
        }
    }

    // for the XMLType field's value write the XML within to a separate file, naming the file with 'key'.xml
    public void processResultSet(String key, String out, int batchSize) {
        try {
            int fileCount = 0;
            int subFolderNum = 1;
            File subFile = new File(out + String.valueOf(subFolderNum));
            subFile.mkdir();
            while (rs.next()) {

                try {
                    Clob xml_clob = rs.getClob("XML");

                    // let's use a reader to stream data
                    Reader char_stream = xml_clob.getCharacterStream();

                    Writer writer = new OutputStreamWriter(new FileOutputStream(out + String.valueOf(subFolderNum) + "/" + rs.getString(key) + ".xml"));

                    Integer c;

                    while ((c = char_stream.read()) != -1) {
                        writer.write(c);
                    }
                    writer.close();
                    char_stream.close();
                    fileCount ++;
                    if (fileCount == batchSize) {
                        subFolderNum++;
                        subFile = new File(out + String.valueOf(subFolderNum));
                        subFile.mkdir();
                        fileCount = 0;
                    }
                } catch (IOException e) {
                    String message = "Error accessing the file: ";
                    System.out.println(message + e.getMessage());
                    e.printStackTrace();
                }
            }
            rs = null;
            stmt.close();
        } catch (SQLException e) {
            String message = "Error looping through results: ";
            System.out.println(message + e.getMessage());
        }
    }

    // This process expects a result from a query to be an integer i.e. select count(*)
    public int processResult() {
        int count = -1;
        try {
            while (rs.next()) {
                count = rs.getInt(1);
            }
            rs = null;
            stmt.close();
        } catch (SQLException e) {
            String message = "Error processing the SQL statement: ";
            System.out.println(message + e.getMessage());
        }
        return count;
    }
}