package uk.gov.homeoffice.dacc.tools;

public interface PostgresDBContract {

	public static final String HOST = "jdbc:postgresql://localhost:5432/";

	public static final String DB_NAME = "postgres";

	public static final String USERNAME = "postgres";

	public static final String PASSWORD = "admin";

}
