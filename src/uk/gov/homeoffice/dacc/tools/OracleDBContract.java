package uk.gov.homeoffice.dacc.tools;

public interface OracleDBContract {

	public static final String HOST = "jdbc:oracle:thin:@localhost:1521:xe";

	public static final String USERNAME = "system";

	public static final String PASSWORD = "admin";

}
