package uk.gov.homeoffice.dacc.tools;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

public class Main {

    private static Properties props = null;
    private static String host = "";
    private static String username = "";
    private static String password = "";
    private static String endDate = "";
    private static String startDate = "";
    private static String updateField = "";
    private static String xmlDataField = "";
    private static String tableName = "";
    private static String outputPath = "";
    private static int rowsCount = -1;
    private static int batchSize = -1;
    private static String logFilePath = "";
    private final static String dateFormat = " 23:59.59','yyyy-mm-dd hh24:mi:ss') ";

    public static void main(String[] args) {

        // load properties from properties file
        setProps();

        // create unique output directory (output + dateTime)
        File subFile = new File(outputPath);
        subFile.mkdir();

        OracleDAO dao = new OracleDAO(host, username, password);

        dao.connectDB();

        // count the number of records in the database table
        countRecords(dao);

        // for every row in the database table, export the XMLType field's data into separate XML files
        exportXMLData(dao);

        writeToLog("Data extraction complete\n");
        writeToLog("===================================================================\n");

        // only if the counts are correct should you update the startDate property to equal the current enddate
        updateProps(endDate);
    }

    private static void setProps() {
        props = new Properties();
        InputStream is = null;

        try {
            is = new FileInputStream("resources/parser.properties");
            props.load(is);
            is.close();
        } catch (FileNotFoundException e) {
            String message = "Can't find the file: ";
            writeToLog(message + e.getMessage() + "\n");
        } catch (IOException e) {
            String message = "Error - Can't load the properties file: ";
            writeToLog(message + e.getMessage() + "\n");
        }

        host = props.getProperty("DB.HOST");
        username = props.getProperty("DB.USERNAME");
        password = props.getProperty("DB.PASSWORD");

        startDate = props.getProperty("QUERY.STARTDATE");
        updateField = props.getProperty("QUERY.UPDATEDATEFIELD");
        xmlDataField = props.getProperty("QUERY.XMLDATAFIELD");
        tableName = props.getProperty("QUERY.TABLENAME");
        outputPath = props.getProperty("DATA.OUTPUTPATH") + getDateTime() + "/";
        batchSize = Integer.valueOf(props.getProperty("DATA.BATCHSIZE"));
        endDate = getYesterdaysDate();

        logFilePath = outputPath + "extract.log";
    }

    private static void updateProps(String d) {
        try {
            FileOutputStream out = new FileOutputStream("resources/parser.properties");
            props.setProperty("QUERY.STARTDATE", d);
            props.store(out, null);
            out.close();
        } catch (FileNotFoundException e) {
            String message = "Can't find the file: ";
            writeToLog(message + e.getMessage() + "\n");
        } catch (IOException e) {
            String message = "Error accessing file: ";
            writeToLog(message + e.getMessage() + "\n");
        }
    }

    private static void countRecords(OracleDAO d) {
        String query = "Select count(*) from " + tableName
                + " where " + updateField + " between to_timestamp('" + startDate + dateFormat
                + " and to_timestamp('" + endDate + dateFormat;

        d.prepareStatement(query);

        rowsCount = d.processResult();

        writeToLog("Record count = " + String.valueOf(rowsCount) + "\n");
    }

    private static void exportXMLData(OracleDAO d) {
        String key = props.getProperty("QUERY.DATAKEY");

        String query = ("SELECT " + key + ", (" + xmlDataField + ").getClobVal() as XML FROM " + tableName
                + " where " + updateField + " between to_timestamp('" + startDate + dateFormat
                + " and to_timestamp('" + endDate + dateFormat);

        writeToLog("Open timestamp window: " + startDate + " 23:59.59\n");
        writeToLog("Close timestamp window: " + endDate + " 23:59.59\n");
        writeToLog("Table name: " + tableName + "\n");

        d.prepareStatement(query);
        d.processResultSet(key, outputPath, batchSize);
    }

    private static String getYesterdaysDate() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(cal.getTime());
    }

    private static String getDateTime() {
        Date datetime = new Date(System.currentTimeMillis());
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
        return dateFormat.format(datetime);
    }

    private static void writeToLog(String data) {
        try {
            File file = new File(logFilePath);
            Path path = Paths.get(logFilePath);
            if (!file.exists()) {
                Files.write(path, data.getBytes(), StandardOpenOption.CREATE);
            } else {
                Files.write(path, data.getBytes(), StandardOpenOption.APPEND);
            }
        } catch (IOException e) {
            String message = "Error accessing the file: ";
            System.out.println(message + e.getMessage());
        }
    }
}
