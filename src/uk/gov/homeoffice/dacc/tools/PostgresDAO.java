package uk.gov.homeoffice.dacc.tools;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;

public class PostgresDAO {

	private static Connection con = null;
	private static ResultSet rs = null;
	private static String outputPath = "data/";

	public static void main(String[] args) {

		try {

			Class.forName("org.postgresql.Driver");
			con = DriverManager.getConnection(PostgresDBContract.HOST + PostgresDBContract.DB_NAME,
					PostgresDBContract.USERNAME, PostgresDBContract.PASSWORD);

			System.out.println("DB connected");

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.out.println("Failed to connect");
		}
		catch (SQLException e) {
			e.printStackTrace();
			System.out.println("Failed to connect");
		}
		PreparedStatement pstmt = null;
		try {
			pstmt = con.prepareStatement("select eventurn,xml_data from solimoc.xml_test;");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			rs = pstmt.executeQuery();

			while (rs.next()) {
				Path path = Paths.get(outputPath + rs.getString("eventurn") + ".xml");

				try {
					Files.write(path, rs.getString("xml_data").getBytes());
					//System.out.println("xml_data: " + rs.getString("xml_data"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				System.out.println("key: " + rs.getString("eventurn"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
