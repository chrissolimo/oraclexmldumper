###Usage:
1. Copy jar and properties resources folder.
2. Edit properties file
3. run java -jar xmldumper.jar

###What's happening?
1. Selects all data from given table at the default start date of 1980-01-01 to yesterday 23:59.59.
2. Creates a log file and adds the ‘count’ of records for the range.
3. Names each file ‘key’.xml and streams the xml from the xmltype field into the file.
4. Once complete the 'enddate' becomes the properties new 'startdate'